package com.amjad.movieapp

import com.amjad.movieapp.data.models.MovieDetailRemote
import com.amjad.movieapp.data.models.MovieDetailsCache
import com.amjad.movieapp.data.models.MovieListRemoteModel
import java.util.*

object TestObjectsProvider {

    fun provideMovieLocalDataModel(id: Int) = MovieDetailsCache(
        id,
        false,
        "",
        0,
        arrayListOf(),
        "",
        "",
        "",
        "",
        "",
        0.1,
        "",
        "",
        0,
        0,
        "",
        "",
        "",
        false,
        0.1,
        1,
        Date().time
    )

    fun provideMovieRemoteModel(id: Int): MovieDetailRemote {
        return MovieDetailRemote(
            false,
            "",
            0,
            arrayListOf(),
            "",
            id,
            "",
            "",
            "",
            "",
            0.1,
            "",
            "",
            0,
            0,
            "",
            "",
            "",
            false,
            1.1,
            1
        )
    }

    fun provideMovieListItem(id: Int,popularity:Double): MovieListRemoteModel {
        return MovieListRemoteModel(
            false,
            "backdrop",
            arrayListOf(),
            id,
            "langauge",
            "title",
            "overview",
            popularity,
            "path",
            "date",
            "title",
            false,
            1.1,
            1
        )
    }
}