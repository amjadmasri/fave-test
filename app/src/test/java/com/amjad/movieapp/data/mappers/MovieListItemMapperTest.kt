package com.amjad.movieapp.data.mappers



import com.amjad.movieapp.TestObjectsProvider
import com.amjad.movieapp.common.utilities.round
import com.amjad.movieapp.data.models.MovieListRemoteModel
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.lang.NullPointerException
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class MovieListItemMapperTest {

    private lateinit var movieListMapper: MovieListMapper

    @Before
    fun setup() {
        movieListMapper = MovieListMapper()

    }

    @Test
    fun checkRemoteToModelValues() {
        val obj = TestObjectsProvider.provideMovieListItem(1,1.1)

        val result = movieListMapper.mapRemoteToModel(obj)

        assertEquals(obj.posterPath, result.poster)
        assertEquals(obj.originalTitle, result.title)
        assertEquals(obj.popularity.round(1), result.popularity,0.1)
        assertEquals(obj.id, result.id)

    }

    @Test
    fun checkRemoteToModelListSize() {
        val obj = TestObjectsProvider.provideMovieListItem(1,1.1)

        val result = movieListMapper.mapRemoteListToModel(arrayListOf(obj))

        assertEquals(1, result.size)

        assertEquals(obj.id, result[0].id)


    }

    @Test(expected = NullPointerException::class)
    fun checkRemoteToModelListThrowsException() {
        val obj :MovieListRemoteModel?= null
        val array = arrayListOf<MovieListRemoteModel>()
        array.add(obj!!)
        val result = movieListMapper.mapRemoteListToModel(array)

    }


}