package com.amjad.movieapp.data.remote

import com.amjad.movieapp.data.models.DiscoverMoviesResponse
import com.amjad.movieapp.data.models.MovieDetailRemote
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class MovieRemoteSourceTest {

    @Mock
    lateinit var apiService: ApiService
    @Mock
    lateinit var discoverMoviesResponse: Response<DiscoverMoviesResponse>

    @Mock
    lateinit var movieDetailsResponse: Response<MovieDetailRemote>

    lateinit var movieRemoteSource: MovieRemoteSourceImp
    @Before
    fun setup() {
        movieRemoteSource = MovieRemoteSourceImp(apiService)

        `when`(apiService.getMovieDetails(ArgumentMatchers.anyInt())).thenReturn(
            Single.just(
                movieDetailsResponse
            )
        )

        `when`(apiService.discoverMovies(ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),page = ArgumentMatchers.anyInt())).thenReturn(
            Single.just(
                discoverMoviesResponse
            )
        )

    }

    @Test
    fun testGetMovieDetailsInvocation() {
        movieRemoteSource.getMovieDetails(1)
        verify(apiService).getMovieDetails(1)
    }

    @Test
    fun testDiscoverMovieInvocation() {
        movieRemoteSource.discoverMovies("",1)
        verify(apiService).discoverMovies("","2019-12-31",1)
    }

    @Test
    fun testGetMovieDetailsResponseType() {
        movieRemoteSource.getMovieDetails(1)
            .test()
            .assertComplete()

        verify(apiService).getMovieDetails(1)
    }

    @Test
    fun testDiscoverMoviesResponseType() {
        movieRemoteSource.discoverMovies("",1)
            .test()
            .assertComplete()

        verify(apiService).discoverMovies("","2019-12-31",1)
    }

    @Test
    fun testDetailsRemoteSourceSuccess() {
        `when`(apiService.getMovieDetails(1)).thenReturn(Single.just(movieDetailsResponse))

        movieRemoteSource.getMovieDetails(1)
            .test()
            .assertValue(movieDetailsResponse)
    }

    @Test
    fun testDiscoverRemoteSourceSuccess() {
        `when`(apiService.discoverMovies("","2019-12-31",1)).thenReturn(Single.just(discoverMoviesResponse))

        movieRemoteSource.discoverMovies("",1)
            .test()
            .assertValue(discoverMoviesResponse)
    }

    @Test
    fun testDetailsRemoteSourceError() {
        val throwable = mock(Throwable::class.java)
        `when`(apiService.getMovieDetails(1)).thenReturn(Single.error(throwable))

        movieRemoteSource.getMovieDetails(1)
            .test()
            .assertError(throwable)
    }


    @Test
    fun testDiscoverRemoteSourceError() {
        val throwable = mock(Throwable::class.java)
        `when`(apiService.discoverMovies("","2019-12-31",1)).thenReturn(Single.error(throwable))

        movieRemoteSource.discoverMovies("",1)
            .test()
            .assertError(throwable)
    }
}