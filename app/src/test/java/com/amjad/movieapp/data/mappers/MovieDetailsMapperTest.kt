package com.amjad.movieapp.data.mappers



import com.amjad.movieapp.TestObjectsProvider
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class MovieDetailsMapperTest {

    private lateinit var movieMapper: MovieDetailsMapper

    @Before
    fun setup() {
        movieMapper = MovieDetailsMapper()

    }

    @Test
    fun checkRemoteToLocalValues() {
        val obj = TestObjectsProvider.provideMovieRemoteModel(1)

        val result = movieMapper.mapRemoteToLocal(obj)

        assertEquals(obj.adult, result.adult)
        assertEquals(obj.backdropPath, result.backdropPath)
        assertEquals(obj.budget, result.budget)
        assertEquals(obj.homepage, result.homepage)
        assertEquals(obj.genres.size, result.genres.size)
        assertEquals(obj.id, result.id)
        assertEquals(obj.imdbId, result.imdbId)
        assertEquals(obj.runtime, result.runtime)
        assertEquals(obj.releaseDate, result.releaseDate)
        assertEquals(obj.title, result.title)
        assertEquals(obj.voteCount, result.voteCount)
        assertEquals(true,result.localCreationDate<=Date().time)
    }

    @Test
    fun checkLocalToDomainValues() {
        val obj = TestObjectsProvider.provideMovieLocalDataModel(1)

        val result = movieMapper.mapLocalToDomain(obj)

        assertEquals(obj.id, result.id)
        assertEquals(obj.backdropPath, result.backDrop)
        assertEquals(obj.runtime, result.duration)
        assertEquals(obj.genres.size, result.genres.size)
        assertEquals(obj.originalLanguage, result.language)
        assertEquals(obj.overview, result.synopsis)
        assertEquals(obj.popularity, result.popularity,0.1)
        assertEquals(1, result.id)
    }
}