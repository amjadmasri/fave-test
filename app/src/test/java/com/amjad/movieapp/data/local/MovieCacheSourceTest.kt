package com.amjad.movieapp.data.local

import com.amjad.movieapp.data.cache.AppDatabase
import com.amjad.movieapp.data.cache.MovieCacheSourceImp
import com.amjad.movieapp.data.cache.dao.MovieDao
import com.amjad.movieapp.data.models.MovieDetailsCache

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class MovieCacheSourceTest {

    private fun <T> anyObject(): T {
        return Mockito.any<T>()
    }

    @Mock
    lateinit var appDatabase: AppDatabase
    @Mock
    lateinit var movieDao: MovieDao

    lateinit var movieCacheSource: MovieCacheSourceImp
    @Before
    fun setup() {
        movieCacheSource = MovieCacheSourceImp(appDatabase)

        Mockito.`when`(appDatabase.movieDao()).thenAnswer {
            movieDao
        }
    }

    @Test
    fun testInsertMovieInvocation() {
        val movieLocalDataModel = MovieDetailsCache(
            1,
            true,
            "path",
            1000,
            arrayListOf(),
            "homepage",
            "imdbId",
            "en",
            "title",
            "overview",
            1.1,
            "path",
            "date",
            123,
            123,
            "status",
            "tagline",
            "title",
            false,
            1.1,
            1,
            Date().time
        )
        movieCacheSource.insertMovieDetails(movieLocalDataModel)

        verify(appDatabase).movieDao()
        verify(movieDao).insert(movieLocalDataModel)
    }

    @Test
    fun testGetMovieByIdInvocation() {

        movieCacheSource.getMovieDetails(1)

        verify(appDatabase).movieDao()
        verify(movieDao).getMovieById(1)
    }
}