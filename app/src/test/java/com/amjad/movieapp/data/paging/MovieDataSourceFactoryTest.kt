package com.amjad.deezerchallange.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.amjad.movieapp.data.paging.DiscoverMoviesPagedDataFactory
import com.amjad.movieapp.data.paging.DiscoverMoviesPagedDataSource
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieDataSourceFactoryTest {

    private lateinit var discoverMoviesPagedDataFactory : DiscoverMoviesPagedDataFactory

    @Mock
    private lateinit var discoverMoviesPagedDataSource: DiscoverMoviesPagedDataSource

    @Mock
    private lateinit var dataSource : MutableLiveData<DiscoverMoviesPagedDataSource>

    @Before
    fun setUp(){
        discoverMoviesPagedDataFactory= DiscoverMoviesPagedDataFactory(discoverMoviesPagedDataSource)
    }


    @Test
    fun testNoExceptionIfCreateIsCalledWithOrderByParameter(){
        discoverMoviesPagedDataFactory.setOrderByParameter("release_date.desc")
        discoverMoviesPagedDataFactory.sourceLiveData=dataSource
        discoverMoviesPagedDataFactory.create()
    }


    @Test
    fun testDataSourceSetParameterIsCalled(){
        discoverMoviesPagedDataFactory.setOrderByParameter("release_date.desc")
        discoverMoviesPagedDataFactory.sourceLiveData=dataSource
        discoverMoviesPagedDataFactory.create()

        verify(discoverMoviesPagedDataSource).setOrderByParameter("release_date.desc")
    }

    @Test
    fun testDataSourceCorrectDataTypeIsReturned(){
        discoverMoviesPagedDataFactory.setOrderByParameter("release_date.desc")
        discoverMoviesPagedDataFactory.sourceLiveData=dataSource
        val obj =discoverMoviesPagedDataFactory.create()

        assertThat(obj,instanceOf(DataSource::class.java))
    }



}