package com.amjad.movieapp.presentation.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.amjad.movieapp.domain.models.MovieDetailsModel
import com.amjad.movieapp.domain.repositories.MovieRepository
import com.amjad.movieapp.common.models.Resource
import javax.inject.Inject

class MovieDetailsViewModel @Inject constructor(private val movieRepository: MovieRepository) :
    ViewModel() {

    fun getMovieDetails(movieId:Int):LiveData<Resource<MovieDetailsModel>>{
        return movieRepository.getMovieDetails(movieId)
    }
}