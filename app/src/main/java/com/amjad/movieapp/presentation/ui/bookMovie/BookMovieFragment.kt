package com.amjad.movieapp.presentation.ui.bookMovie


import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.amjad.movieapp.R
import com.amjad.movieapp.presentation.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_book_movie.*


class BookMovieFragment : BaseFragment() {


    override fun getLayoutRes(): Int = R.layout.fragment_book_movie

    override fun attachFragmentInteractionListener(context: Context) {
    }


    private lateinit var navController: NavController



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        val bookMovieLink = BookMovieFragmentArgs.fromBundle(arguments!!).bookMovieLink

        book_movie_webview.settings.javaScriptEnabled = true
        book_movie_webview.settings.allowFileAccessFromFileURLs=true
        book_movie_webview.loadUrl(bookMovieLink)


    }



}
