package com.amjad.movieapp.presentation.ui.discoverMovies

import androidx.recyclerview.widget.DiffUtil
import com.amjad.movieapp.domain.models.MovieListModel
import javax.inject.Inject

class MovieDiffCallBacks @Inject constructor() : DiffUtil.ItemCallback<MovieListModel>() {
    override fun areItemsTheSame(
        oldItem: MovieListModel,
        newItem: MovieListModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: MovieListModel,
        newItem: MovieListModel
    ): Boolean {
        return oldItem == newItem
    }
}