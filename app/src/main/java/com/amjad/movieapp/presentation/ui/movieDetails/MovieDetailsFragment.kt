package com.amjad.movieapp.presentation.ui.movieDetails


import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.amjad.movieapp.R
import com.amjad.movieapp.common.utilities.AppConstants
import com.amjad.movieapp.data.models.Genre
import com.amjad.movieapp.domain.models.MovieDetailsModel
import com.amjad.movieapp.presentation.ui.base.BaseFragment
import com.amjad.movieapp.presentation.viewModels.MovieDetailsViewModel
import com.amjad.movieapp.presentation.viewModels.ViewModelProviderFactory
import com.amjad.movieapp.common.models.Status
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_movie_details.*
import java.util.*
import javax.inject.Inject


class MovieDetailsFragment : BaseFragment() {


    override fun getLayoutRes(): Int = R.layout.fragment_movie_details

    override fun attachFragmentInteractionListener(context: Context) {
    }

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory


    private lateinit var viewModel: MovieDetailsViewModel


    private lateinit var navController: NavController


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()

        navController = Navigation.findNavController(view)

        viewModel = ViewModelProvider(this, viewModelProviderFactory)
            .get(MovieDetailsViewModel::class.java)

        val movieId = MovieDetailsFragmentArgs.fromBundle(arguments!!).movieId

        viewModel.getMovieDetails(movieId)
            .observe(viewLifecycleOwner, Observer {
                when(it.status){
                    Status.SUCCESS -> {
                        showLoading(false)
                        render(it.data!!)
                    }
                    Status.ERROR -> {
                        showLoading(false)
                        Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING-> showLoading(true)

                }
            })

    }

    private fun showLoading(showLoading: Boolean) {
        if(showLoading)
            loading_bar.visibility=View.VISIBLE
        else
            loading_bar.visibility=View.GONE
    }

    private fun render(data: MovieDetailsModel) {
        movie_title.text = data.title
        movie_language.text = data.language
        movie_duration.text = data.duration?.toString()
        movie_synopsis.text = data.synopsis
        movie_popularity.text = data.popularity.toString()

        Glide.with(movie_backdrop)
            .load(AppConstants.BASE_BACKDROP_PATH + data.backDrop)
            .placeholder(R.drawable.ic_image_black_24dp)
            .centerCrop()
            .into(movie_backdrop)

        val activity = activity as AppCompatActivity
        activity.supportActionBar?.title = data.title

        setupGenreLayout(data.genres)

        book_movie_button.setOnClickListener {
            navController.navigate(
                MovieDetailsFragmentDirections.actionMovieDetailsFragmentToBookMovieFragment(
                    "https://www.cathaycineplexes.com.sg/"
                )
            )
        }
    }


    private fun setupGenreLayout(genres: List<Genre>) {
        val genreMap = HashMap<String, Int>()
        val genresNames = ArrayList<String>()
        for (genre in genres) {
            genreMap[genre.name] = genre.id
            genresNames.add(genre.name)
        }
        tagContainerLayout.tags = genresNames

    }


    private fun setupUi() {


    }


}
