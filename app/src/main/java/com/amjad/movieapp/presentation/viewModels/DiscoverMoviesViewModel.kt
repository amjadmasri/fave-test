package com.amjad.movieapp.presentation.viewModels

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.amjad.movieapp.domain.models.MovieListModel
import com.amjad.movieapp.domain.repositories.MovieRepository
import com.amjad.movieapp.common.models.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DiscoverMoviesViewModel @Inject constructor(private val moviesRepository: MovieRepository):
    ViewModel() {

    private var movieList : MutableLiveData<PagedList<MovieListModel>>?=null
    private var networkState: MediatorLiveData<Resource<String>> = MediatorLiveData()

    /**
     * gets the paged listing with the discover movies data in it
     * and observes the network state live data and posts the value to the UI
     * Subscribes to the rxPagedList and posts the value the the live data of pagedList
     * only calls the repo to get new data if the call is happening for the first time
     * or if the UI client requests that the data should be refreshed
     * this way the UI observing the movieList can change configuration and still have the same data without needing to make a new
     * call to the repository
     * @param orderBy String
     * @param shouldRefresh Boolean
     * @return LiveData<PagedList<MovieListModel>>
     */
    @SuppressLint("CheckResult")
    fun discoverMovies(orderBy:String,shouldRefresh:Boolean): LiveData<PagedList<MovieListModel>> {
        if(movieList==null||shouldRefresh){
            movieList=MutableLiveData()
            val pagedListing = moviesRepository.discoverMovies(orderBy,1)
            networkState.addSource(pagedListing.networkState) {
                networkState.postValue(it)
            }
                pagedListing.pagedList.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    movieList!!.postValue(it)
                },{

                })
        }
        return movieList!!
    }

    /**
     * return a LiveData instead of MutableLiveData so that the UI can't change the value since LiveData is immutable
     * and MutableLiveData is not
     * @return LiveData<Resource<String>>
     */
    fun observeNetworkState():LiveData<Resource<String>>{
        return networkState
    }
}