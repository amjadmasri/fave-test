package com.amjad.movieapp.presentation.ui.discoverMovies


import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import com.amjad.movieapp.R
import com.amjad.movieapp.domain.models.MovieListModel
import com.amjad.movieapp.presentation.ui.base.BaseFragment
import com.amjad.movieapp.presentation.viewModels.DiscoverMoviesViewModel
import com.amjad.movieapp.presentation.viewModels.ViewModelProviderFactory
import com.amjad.movieapp.common.models.Status
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_discover_movies.*
import javax.inject.Inject


class DiscoverMoviesFragment : BaseFragment(), MoviesPagedAdapter.MovieAdapterListener {


    override fun getLayoutRes(): Int = R.layout.fragment_discover_movies

    override fun attachFragmentInteractionListener(context: Context) {
    }

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    @Inject
    lateinit var moviesPagedAdapter: MoviesPagedAdapter


    private lateinit var viewModel: DiscoverMoviesViewModel

    lateinit var gridLayoutManager: GridLayoutManager

    @Inject
    lateinit var gson: Gson

    private lateinit var navController: NavController


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()

        navController = Navigation.findNavController(view)

        viewModel = ViewModelProvider(this, viewModelProviderFactory)
            .get(DiscoverMoviesViewModel::class.java)


        viewModel.observeNetworkState().observe(viewLifecycleOwner, Observer {
            when(it.status){
                Status.SUCCESS-> removeLoading()
                Status.LOADING->showLoading()
                Status.ERROR->showError(it.message)
            }
        })


        val discoverMoviesLiveData = viewModel.discoverMovies("release_date.desc", false)

        observeDiscoverMovies(discoverMoviesLiveData)
    }

    private fun observeDiscoverMovies(discoverMoviesLiveData: LiveData<PagedList<MovieListModel>>) {
        discoverMoviesLiveData.observe(viewLifecycleOwner, Observer {
            moviesPagedAdapter.submitList(it)
            swiperefresh.isRefreshing = false
        })
    }


    private fun setupUi() {
        gridLayoutManager = GridLayoutManager(activity, 2)
        movies_recycler.layoutManager = gridLayoutManager
        movies_recycler.adapter = moviesPagedAdapter

        moviesPagedAdapter.listener = this


        swiperefresh.setOnRefreshListener {
            moviesPagedAdapter.submitList(null)
            val discoverMovieLiveData = viewModel.discoverMovies("release_date.desc", true)
            observeDiscoverMovies(discoverMovieLiveData)

        }

        floatingActionButton.setOnClickListener {
            gridLayoutManager.smoothScrollToPosition(movies_recycler, null, 0)
        }

    }

    override fun onMovieClick(movieListModel: MovieListModel?) {

        navController.navigate(
            DiscoverMoviesFragmentDirections.actionDiscoverMoviesFragmentToMovieDetailsFragment(
                movieListModel!!.id
            )
        )
    }


    private fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    private fun removeLoading(){
        loading.visibility=View.GONE
    }

    private fun showError(message:String?){
        removeLoading()
        Toast.makeText(activity, message?:"there was an error", Toast.LENGTH_LONG).show()
    }

}
