package com.amjad.movieapp.presentation.ui.discoverMovies

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amjad.movieapp.R
import com.amjad.movieapp.common.utilities.AppConstants
import com.amjad.movieapp.domain.models.MovieListModel
import com.bumptech.glide.Glide
import javax.inject.Inject


class MoviesPagedAdapter @Inject constructor(movieDiffCallBacks: MovieDiffCallBacks) :
    PagedListAdapter<MovieListModel, MoviesPagedAdapter.DiscoverMovieViewHolder>(
        movieDiffCallBacks
    ) {

    lateinit var listener: MovieAdapterListener

    override fun onBindViewHolder(holder: DiscoverMovieViewHolder, position: Int) {
        val movieListModel: MovieListModel? = getItem(position)

        holder.bindTo(movieListModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiscoverMovieViewHolder =
        DiscoverMovieViewHolder(parent, listener)


    interface MovieAdapterListener {

        fun onMovieClick(movieListModel: MovieListModel?)
    }


    class DiscoverMovieViewHolder(parent: ViewGroup, private val listener: MovieAdapterListener) :
        RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_list_item, parent, false)
        ) {
        private val movieItem = itemView.findViewById<ConstraintLayout>(R.id.movie_item_layout)
        private val movieTitleText = itemView.findViewById<TextView>(R.id.item_movie_title)
        private val moviePopularity = itemView.findViewById<TextView>(R.id.item_movie_popularity)
        private val moviePoster = itemView.findViewById<ImageView>(R.id.item_poster_post)

        fun bindTo(movieListModel: MovieListModel?) {
            movieTitleText.text = movieListModel?.title

            Glide.with(moviePoster)
                .load(AppConstants.BASE_POSTER_PATH + movieListModel?.poster)
                .placeholder(R.drawable.ic_image_black_24dp)
                .centerCrop()
                .into(moviePoster)
            moviePopularity.text=moviePopularity.resources.getText(R.string.popularity)
            moviePopularity.text = moviePopularity.text.toString() + movieListModel?.popularity

            movieItem.setOnClickListener {
                listener.onMovieClick(movieListModel)
            }

        }
    }
}