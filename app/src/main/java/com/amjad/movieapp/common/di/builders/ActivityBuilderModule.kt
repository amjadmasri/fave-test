package com.amjad.movieapp.common.di.builders

import com.amjad.movieapp.presentation.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module responsible for giving the app activities the ability to be have fields with the @Inject annotation
 * and injecting classes instances into them
 */
@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun bindMainActivity(): MainActivity
}