package com.amjad.movieapp.common.di.interfaces

import javax.inject.Qualifier

/**
 * annotation for injecting a string that represents the format of the dates string that Gson can use for handling date string from APIS
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DateFormatInfo