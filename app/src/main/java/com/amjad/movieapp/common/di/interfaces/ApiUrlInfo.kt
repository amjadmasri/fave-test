package com.amjad.movieapp.common.di.interfaces

import javax.inject.Qualifier

/**
 * annotation for injecting a string that represents the API BASE URL of the movieDB API
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiUrlInfo