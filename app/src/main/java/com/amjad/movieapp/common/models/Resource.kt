package com.amjad.movieapp.common.models

/**
 * A data class that wraps around any model/resource with the ability to give one of three state (Success,Loading,Error)
 * @param out T
 * @property status Status
 * @property data T?
 * @property message String?
 * @constructor
 */
data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(msg: String, data: T? = null): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                msg
            )
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(
                Status.LOADING,
                data,
                null
            )
        }
    }
}
