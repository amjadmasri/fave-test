package com.amjad.movieapp.common.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.amjad.movieapp.common.di.interfaces.ViewModelKey
import com.amjad.movieapp.presentation.viewModels.DiscoverMoviesViewModel
import com.amjad.movieapp.presentation.viewModels.MovieDetailsViewModel
import com.amjad.movieapp.presentation.viewModels.ViewModelProviderFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * The module responsible for providing and binding all the Viewmodels of the application
 */

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(DiscoverMoviesViewModel::class)
    abstract fun bindDiscoverMoviesViewModel(discoverMoviesViewModel: DiscoverMoviesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailsViewModel::class)
    abstract fun bindMovieDetailsViewModel(movieDetailsViewModel: MovieDetailsViewModel): ViewModel

}