package com.amjad.movieapp.common.di.components

import com.amjad.movieapp.common.MovieApp
import com.amjad.movieapp.common.di.builders.ActivityBuilderModule
import com.amjad.movieapp.common.di.builders.FragmentBuilderModule
import com.amjad.movieapp.common.di.modules.AppModule
import com.amjad.movieapp.common.di.modules.ViewModelModule
import com.amjad.movieapp.data.di.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * The Application component that has all the modules of the application in it
 */

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        FragmentBuilderModule::class,
        ViewModelModule::class,
        DataModule::class,
        ActivityBuilderModule::class]
)
interface AppComponent : AndroidInjector<MovieApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MovieApp): Builder

        fun build(): AppComponent
    }

    override fun inject(app: MovieApp)
}