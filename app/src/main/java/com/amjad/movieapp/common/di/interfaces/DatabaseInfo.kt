package com.amjad.movieapp.common.di.interfaces

import javax.inject.Qualifier

/**
 * annotation for injecting a string that represents the local sqlite database name
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DatabaseInfo