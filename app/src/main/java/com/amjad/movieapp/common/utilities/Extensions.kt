package com.amjad.movieapp.common.utilities

/**
 * file that holds all the kotlin extensions that are written for this application
 */


/**
 * method that rounds a double value by a decimal point given as a parameter
 * @receiver Double
 * @param decimals Int
 * @return Double
 */

fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return kotlin.math.round(this * multiplier) / multiplier
}