package com.amjad.movieapp.common.utilities

/**
 * string constants of the application like base API url and images Base url
 */
class AppConstants {
    companion object{
        const val DB_NAME="movie_app.db"
        const val BASE_POSTER_PATH= "https://image.tmdb.org/t/p/w342"
        const val BASE_BACKDROP_PATH = "https://image.tmdb.org/t/p/w780"
        const val API_BASE_URL = "https://api.themoviedb.org/3/"
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        const val API_KEY ="328c283cd27bd1877d9080ccb1604c91"
    }

}