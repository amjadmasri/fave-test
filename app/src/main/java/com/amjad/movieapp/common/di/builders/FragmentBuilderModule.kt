package com.amjad.movieapp.common.di.builders


import com.amjad.movieapp.presentation.ui.bookMovie.BookMovieFragment
import com.amjad.movieapp.presentation.ui.discoverMovies.DiscoverMoviesFragment
import com.amjad.movieapp.presentation.ui.movieDetails.MovieDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module responsible for giving the app Fragments the ability to be have fields with the @Inject annotation
 * and injecting classes instances into them
 */
@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector()
    abstract fun contributeDiscoverMoviesFragment(): DiscoverMoviesFragment

    @ContributesAndroidInjector()
    abstract fun contributeMovieDetailsFragment(): MovieDetailsFragment

    @ContributesAndroidInjector()
    abstract fun contributeBookMovieFragment(): BookMovieFragment
}