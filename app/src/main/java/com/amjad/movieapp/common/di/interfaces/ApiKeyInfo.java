package com.amjad.movieapp.common.di.interfaces;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * annotation for injecting a string that represents the API KEY of the movieDB API
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiKeyInfo {
}
