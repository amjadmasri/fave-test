package com.amjad.movieapp.common.utilities;

import com.amjad.movieapp.common.di.interfaces.ApiKeyInfo;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * an OKHTTP interceptor that adds GET parameter and adds headers to all outgoing network requests
 */
public class QueryParametersInterceptor implements Interceptor {

    private final String apiKey;

    @Inject
    public QueryParametersInterceptor(@ApiKeyInfo String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", apiKey)
                .build();

        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("Content-Type", "application/json")
                .url(url);


        Request request = requestBuilder.build();
        return chain.proceed(request);
    }

}
