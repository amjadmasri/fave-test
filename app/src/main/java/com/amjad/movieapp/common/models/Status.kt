package com.amjad.movieapp.common.models

/**
 * the Status of any Resource wrapped model
 */
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
