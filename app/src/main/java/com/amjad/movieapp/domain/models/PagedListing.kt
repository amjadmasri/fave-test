package com.amjad.movieapp.domain.models

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.amjad.movieapp.common.models.Resource
import io.reactivex.Observable

/**
 * a utility model that holds a reference to a paged list used by the paged adapter of the paging library
 * as well as a live data that holds a resource that represents the state of the network requests that the paging library calls
 * @param T
 * @property pagedList Observable<PagedList<T>>
 * @property networkState LiveData<Resource<String>>
 * @constructor
 */
data class PagedListing<T>(
    // the LiveData of paged lists for the UI to observe
    val pagedList: Observable<PagedList<T>>,
    // represents the network request status to show to the user
    val networkState: LiveData<Resource<String>>
)
