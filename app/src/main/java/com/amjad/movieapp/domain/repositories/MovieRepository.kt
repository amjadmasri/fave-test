package com.amjad.movieapp.domain.repositories

import androidx.lifecycle.LiveData
import com.amjad.movieapp.domain.models.MovieDetailsModel
import com.amjad.movieapp.domain.models.MovieListModel
import com.amjad.movieapp.domain.models.PagedListing
import com.amjad.movieapp.common.models.Resource

/**
 * interface that defines the operations of the movie repository
 * discover movies that gets a list of movies in pages
 * get movie details when a movie from a list is clicked
 */
interface MovieRepository {

    fun discoverMovies(orderBy:String,page:Int) : PagedListing<MovieListModel>

    fun getMovieDetails(movieId:Int): LiveData<Resource<MovieDetailsModel>>
}