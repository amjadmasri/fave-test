package com.amjad.movieapp.domain.models

import com.amjad.movieapp.data.models.Genre

/**
 * data class representing all the data that the UI needs to present in the movie details screen
 * @property id Int
 * @property synopsis String?
 * @property genres List<Genre>
 * @property language String
 * @property duration Int?
 * @property title String
 * @property backDrop String?
 * @property popularity Double
 * @constructor
 */
data class MovieDetailsModel (
    val id:Int,
    val synopsis:String?,
    val genres:List<Genre>,
    val language:String,
    val duration :Int?,
    val title:String,
    val backDrop:String?,
    val popularity:Double
)