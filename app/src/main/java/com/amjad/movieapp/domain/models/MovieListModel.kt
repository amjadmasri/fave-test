package com.amjad.movieapp.domain.models

/**
 * data class that has the data the discover movie screen's UI needs to present
 * @property poster String?
 * @property title String
 * @property popularity Double
 * @property id Int
 * @constructor
 */
data class MovieListModel (
    val poster:String?,
    val title:String,
    val popularity:Double,
    val id:Int
    )
