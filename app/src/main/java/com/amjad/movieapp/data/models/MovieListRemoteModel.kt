package com.amjad.movieapp.data.models


import com.google.gson.annotations.SerializedName

/**
 * The movie model that represents an item of a list of the movie resource as returned from the discover movies API
 * @property adult Boolean
 * @property backdropPath String
 * @property genreIds List<Int>
 * @property id Int
 * @property originalLanguage String
 * @property originalTitle String
 * @property overview String
 * @property popularity Double
 * @property posterPath String
 * @property releaseDate String
 * @property title String
 * @property video Boolean
 * @property voteAverage Double
 * @property voteCount Int
 * @constructor
 */
data class MovieListRemoteModel(
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdropPath: String,
    @SerializedName("genre_ids")
    val genreIds: List<Int>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("poster_path")
    val posterPath: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("video")
    val video: Boolean,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Int
)