package com.amjad.movieapp.data.cache

import androidx.lifecycle.LiveData
import com.amjad.movieapp.data.models.MovieDetailsCache
import io.reactivex.Completable
import javax.inject.Inject

/**
 * implementation of the local cache operations on the local movie resource using the Room database instance
 * @property appDatabase AppDatabase
 * @constructor
 */
class MovieCacheSourceImp @Inject constructor(private val appDatabase: AppDatabase):MovieCacheSource{
    override fun insertMovieDetails(movieDetailsCache: MovieDetailsCache): Completable {
       return appDatabase.movieDao().insert(movieDetailsCache)
    }

    override fun getMovieDetails(movieId: Int): LiveData<MovieDetailsCache> {
       return appDatabase.movieDao().getMovieById(movieId)
    }
}