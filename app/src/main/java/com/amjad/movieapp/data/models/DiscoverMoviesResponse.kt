package com.amjad.movieapp.data.models


import com.google.gson.annotations.SerializedName

/**
 * The network response of the discover movie API
 * @property page Int
 * @property results List<MovieListRemoteModel>
 * @property totalPages Int
 * @property totalResults Int
 * @constructor
 */
data class DiscoverMoviesResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<MovieListRemoteModel>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)