package com.amjad.movieapp.data.remote

import com.amjad.movieapp.data.models.DiscoverMoviesResponse
import com.amjad.movieapp.data.models.MovieDetailRemote
import com.amjad.movieapp.data.models.MovieListRemoteModel
import io.reactivex.Single
import retrofit2.Response

/**
 * interface of the operations on the remote service of the movie resource
 */
interface MovieRemoteSource {

    fun discoverMovies(orderBy:String,page:Int):Single<Response<DiscoverMoviesResponse>>

    fun getMovieDetails(movieId:Int):Single<Response<MovieDetailRemote>>
}