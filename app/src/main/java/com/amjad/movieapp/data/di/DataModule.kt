package com.amjad.movieapp.data.di

import com.amjad.movieapp.data.cache.MovieCacheSource
import com.amjad.movieapp.data.cache.MovieCacheSourceImp
import com.amjad.movieapp.data.remote.MovieRemoteSource
import com.amjad.movieapp.data.remote.MovieRemoteSourceImp
import com.amjad.movieapp.data.repositories.MovieRepositoryImp
import com.amjad.movieapp.domain.repositories.MovieRepository
import dagger.Module
import dagger.Provides

/**
 * Module responsible for binding the interfaces of the repositories , remote and cache source to the concrete instance
 * that use the Room Database and the Retrofit DataBase
 * can switch the implementation here to swap the concrete instance with another that implements the same Interface
 */
@Module
class DataModule {

    @Provides
    fun provideMoviesRepository(moviesRepositoryImp: MovieRepositoryImp): MovieRepository =
        moviesRepositoryImp

    @Provides
    fun provideMovieRemoteSource(movieRemoteSourceImp: MovieRemoteSourceImp): MovieRemoteSource =
        movieRemoteSourceImp


    @Provides
    fun provideMovieCacheSource(movieCacheSourceImp: MovieCacheSourceImp): MovieCacheSource =
        movieCacheSourceImp
}