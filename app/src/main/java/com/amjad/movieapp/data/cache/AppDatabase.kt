package com.amjad.movieapp.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.amjad.movieapp.data.cache.converters.GenreConverter
import com.amjad.movieapp.data.cache.dao.MovieDao
import com.amjad.movieapp.data.models.MovieDetailsCache

/**
 * The Room Database Instance for the movie application
 * provide the daos of the application
 */
@Database(entities = [MovieDetailsCache::class], version = 1)
@TypeConverters(value = [GenreConverter::class])
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}