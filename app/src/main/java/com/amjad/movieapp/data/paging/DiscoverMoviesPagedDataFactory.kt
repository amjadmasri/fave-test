package com.amjad.movieapp.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.amjad.movieapp.domain.models.MovieListModel
import javax.inject.Inject

/**
 * The factory needed by the Paging library of the Android Architectural components
 * note: need to set the orderBy parameter if needed before building the pagedList
 * @property discoverMoviesPagedDataSource DiscoverMoviesPagedDataSource
 * @property orderBy String
 * @property sourceLiveData MutableLiveData<DiscoverMoviesPagedDataSource>
 * @constructor
 */
class DiscoverMoviesPagedDataFactory @Inject constructor(private val discoverMoviesPagedDataSource: DiscoverMoviesPagedDataSource)
    : DataSource.Factory<Int, MovieListModel>() {
    private lateinit var orderBy: String
    var sourceLiveData = MutableLiveData<DiscoverMoviesPagedDataSource>()

    override fun create(): DataSource<Int, MovieListModel> {
        discoverMoviesPagedDataSource.setOrderByParameter(orderBy)
        sourceLiveData.postValue(discoverMoviesPagedDataSource)
        return discoverMoviesPagedDataSource
    }

    fun setOrderByParameter(orderBy:String){
        this.orderBy=orderBy
    }
}