package com.amjad.movieapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.RxPagedListBuilder
import com.amjad.movieapp.data.cache.MovieCacheSource
import com.amjad.movieapp.data.mappers.MovieDetailsMapper
import com.amjad.movieapp.data.models.MovieDetailRemote
import com.amjad.movieapp.data.models.MovieDetailsCache
import com.amjad.movieapp.data.models.NetworkBoundResource
import com.amjad.movieapp.data.models.isExpiredAfterOneDay
import com.amjad.movieapp.data.paging.DiscoverMoviesPagedDataFactory
import com.amjad.movieapp.data.remote.MovieRemoteSource
import com.amjad.movieapp.domain.models.MovieDetailsModel
import com.amjad.movieapp.domain.models.MovieListModel
import com.amjad.movieapp.domain.models.PagedListing
import com.amjad.movieapp.domain.repositories.MovieRepository
import com.amjad.movieapp.common.models.Resource
import com.amjad.movieapp.common.models.Status
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

/**
 * implementation of the operations of the repository of the movie resource
 * @property discoverMoviesPagedDataFactory DiscoverMoviesPagedDataFactory
 * @property movieRemoteSource MovieRemoteSource
 * @property movieCacheSource MovieCacheSource
 * @property movieDetailsMapper MovieDetailsMapper
 * @constructor
 */
class MovieRepositoryImp @Inject constructor(private val discoverMoviesPagedDataFactory: DiscoverMoviesPagedDataFactory,private val movieRemoteSource: MovieRemoteSource,private val movieCacheSource: MovieCacheSource,private val movieDetailsMapper: MovieDetailsMapper): MovieRepository{
    /**
     * Builds the PagedList of the paging library using the factorySource
     * and returns an instance of the PagedListing Class that holds a reference to the PagedList
     * and an instance of a liveData that holds the networks state of the network requests done by the Paging library data source
     * @param orderBy String
     * @param page Int
     * @return PagedListing<MovieListModel>
     */
    override fun discoverMovies(orderBy: String, page: Int) :PagedListing<MovieListModel>{
        discoverMoviesPagedDataFactory.setOrderByParameter(orderBy)
        val list = RxPagedListBuilder(discoverMoviesPagedDataFactory, 20)
            .buildObservable()

        return PagedListing(
            pagedList = list,
            networkState = Transformations.switchMap(discoverMoviesPagedDataFactory.sourceLiveData) {
                it.networkState
            })
    }

    /**
     * handles the flow of getting the details of a movie by its ID
     * either getting the cached data or getting the remote data if 1- data is not cached 2- data was in cache for more than 24 hours
     * @param movieId Int
     * @return LiveData<Resource<MovieDetailsModel>>
     */
    override fun getMovieDetails(movieId: Int) :LiveData<Resource<MovieDetailsModel>>{
        val result: LiveData<Resource<MovieDetailsCache>> = object : NetworkBoundResource<MovieDetailsCache, MovieDetailRemote>(){
            override fun saveCallResult(item: MovieDetailRemote): Completable {
                return movieCacheSource.insertMovieDetails(movieDetailsMapper.mapRemoteToLocal(item))
            }

            override fun loadFromDb(): LiveData<MovieDetailsCache> {

                return movieCacheSource.getMovieDetails(movieId)
            }

            override fun createCall(): Single<Response<MovieDetailRemote>> {
                return movieRemoteSource.getMovieDetails(movieId)
            }

            override fun shouldFetch(data: MovieDetailsCache?): Boolean {
                return data?.isExpiredAfterOneDay() ?: true
            }
        }.asLiveData
        return Transformations.map(result){ input ->

            mapLocalToDomain(input)
        }
    }

    private fun mapLocalToDomain(input: Resource<MovieDetailsCache>): Resource<MovieDetailsModel> {
        return when(input.status){
            Status.SUCCESS-> Resource.success(input.data?.let { movieDetailsMapper.mapLocalToDomain(it) })
            Status.LOADING-> Resource.loading()
            Status.ERROR-> Resource.error(input.message.toString())
        }
    }

}