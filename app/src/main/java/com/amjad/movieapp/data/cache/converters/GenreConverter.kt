package com.amjad.movieapp.data.cache.converters

import androidx.room.TypeConverter
import com.amjad.movieapp.data.models.Genre
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * a Room converter that takes the Genre model and saves into room the JSON string representation of the model
 * and can convert the JSON representation from room back to an instance of the Genre model
 */

object GenreConverter {
    @JvmStatic
    @TypeConverter
    fun fromString(value: String?): List<Genre> {
        return Gson().fromJson<List<Genre>>(
            value,
            object : TypeToken<List<Genre?>?>() {}.type
        )
    }

    @JvmStatic
    @TypeConverter
    fun fromList(genre: List<Genre?>?): String {
        return Gson().toJson(genre)
    }
}

