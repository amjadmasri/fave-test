package com.amjad.movieapp.data.mappers

import com.amjad.movieapp.common.utilities.round
import com.amjad.movieapp.data.models.MovieListRemoteModel
import com.amjad.movieapp.domain.models.MovieListModel
import javax.inject.Inject

/**
 * A utility class that maps the remote movie list item to the domain movie list model
 * as one instance or as a list
 */
class MovieListMapper @Inject constructor() {

    fun mapRemoteToModel(movieListRemoteModel: MovieListRemoteModel): MovieListModel {
        return MovieListModel(
            movieListRemoteModel.posterPath,
            movieListRemoteModel.originalTitle,
            movieListRemoteModel.popularity.round(1),
            movieListRemoteModel.id
        )
    }

    fun mapRemoteListToModel(list: List<MovieListRemoteModel>): List<MovieListModel> {
        return list.map {
            mapRemoteToModel(it)
        }
    }



}