package com.amjad.movieapp.data.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.amjad.movieapp.data.models.MovieDetailsCache
import io.reactivex.Completable

/**
 * Room Dao that handles the local cache operation of the movie table
 * insert a single instance of a movie details
 * insert a list of instances of movie details
 * get a certain movie by its id
 */
@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertList(movieDetailsCacheList: List<MovieDetailsCache>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movieDetailsCache: MovieDetailsCache): Completable

    @Query("SELECT * from movies where  id=:id")
    fun getMovieById(id: Int): LiveData<MovieDetailsCache>

}