package com.amjad.movieapp.data.remote

import com.amjad.movieapp.data.models.DiscoverMoviesResponse
import com.amjad.movieapp.data.models.MovieDetailRemote
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

/**
 * implementation of the operations on the remote service of the movie resource
 * Using the APIService of the Retrofit library
 * @property apiService ApiService
 * @constructor
 */
class MovieRemoteSourceImp @Inject constructor(private val apiService: ApiService) :
    MovieRemoteSource {
    override fun discoverMovies(
        orderBy: String,
        page: Int
    ): Single<Response<DiscoverMoviesResponse>> = apiService.discoverMovies(orderBy, page = page)

    override fun getMovieDetails(movieId: Int): Single<Response<MovieDetailRemote>> =
        apiService.getMovieDetails(movieId)
}