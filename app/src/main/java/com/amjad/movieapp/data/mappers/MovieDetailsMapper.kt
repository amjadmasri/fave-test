package com.amjad.movieapp.data.mappers

import com.amjad.movieapp.data.models.MovieDetailRemote
import com.amjad.movieapp.data.models.MovieDetailsCache
import com.amjad.movieapp.domain.models.MovieDetailsModel
import java.util.*
import javax.inject.Inject

/**
 * A utility class that is responsible for mapping the model in the data (remote to local cache and vice versa)
 */
class MovieDetailsMapper @Inject constructor() {

    fun mapRemoteToLocal(remote: MovieDetailRemote): MovieDetailsCache {
        return MovieDetailsCache(
            remote.id,
            remote.adult,
            remote.backdropPath,
            remote.budget,
            remote.genres,
            remote.homepage,
            remote.imdbId,
            remote.originalLanguage,
            remote.originalTitle,
            remote.overview,
            remote.popularity,
            remote.posterPath,
            remote.releaseDate,
            remote.revenue,
            remote.runtime,
            remote.status,
            remote.tagline,
            remote.title,
            remote.video,
            remote.voteAverage,
            remote.voteCount,
            Date().time
        )
    }

    fun mapLocalToDomain(local: MovieDetailsCache): MovieDetailsModel {
        return MovieDetailsModel(
            local.id,
            local.overview, local.genres, local.originalLanguage, local.runtime, local.originalTitle,local.backdropPath,local.popularity
        )
    }
}