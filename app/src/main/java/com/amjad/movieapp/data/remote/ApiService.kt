package com.amjad.movieapp.data.remote

import com.amjad.movieapp.data.models.DiscoverMoviesResponse
import com.amjad.movieapp.data.models.MovieDetailRemote
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("discover/movie?with_original_language=en")
    fun discoverMovies(@Query("sort_by") orderBy:String,@Query("release_date.lte")releaseDateBefore:String ="2019-12-31",@Query("page")page:Int):Single<Response<DiscoverMoviesResponse>>

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") movieId:Int):Single<Response<MovieDetailRemote>>

}