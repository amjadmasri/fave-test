package com.amjad.movieapp.data.cache

import androidx.lifecycle.LiveData
import com.amjad.movieapp.data.models.MovieDetailsCache
import io.reactivex.Completable

/**
 * interface of the operations on the local cache of the movie resource
 */
interface MovieCacheSource {

    fun insertMovieDetails(movieDetailsCache: MovieDetailsCache):Completable

    fun getMovieDetails(movieId:Int):LiveData<MovieDetailsCache>
}