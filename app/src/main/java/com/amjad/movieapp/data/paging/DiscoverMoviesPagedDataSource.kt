package com.amjad.movieapp.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.amjad.movieapp.data.mappers.MovieListMapper
import com.amjad.movieapp.data.models.DiscoverMoviesResponse
import com.amjad.movieapp.data.remote.MovieRemoteSource
import com.amjad.movieapp.domain.models.MovieListModel
import com.amjad.movieapp.common.models.Resource
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

/**
 * The dataSource that is needed by the Paging library of the Android Architectural Components
 * note: if the orderby parameter of the discover API is needed, setting the parameter should be done before returning the new instance to the factory
 * @property movieRemoteSource MovieRemoteSource
 * @property movieListMapper MovieListMapper
 * @property orderBy String
 * @property networkState MutableLiveData<Resource<String>>
 * @constructor
 */
class DiscoverMoviesPagedDataSource @Inject constructor(
    private val movieRemoteSource: MovieRemoteSource,
    private val movieListMapper: MovieListMapper
) :
    PageKeyedDataSource<Int, MovieListModel>() {

    private lateinit var orderBy: String
    val networkState = MutableLiveData<Resource<String>>()

    fun setOrderByParameter(orderBy: String) {
        this.orderBy = orderBy
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, MovieListModel>
    ) {
        networkState.postValue(Resource.loading())
        movieRemoteSource.discoverMovies(orderBy, 1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : SingleObserver<Response<DiscoverMoviesResponse>> {
                override fun onSuccess(response: Response<DiscoverMoviesResponse>) {
                    if (response.isSuccessful) {
                        val data = response.body()!!
                        val items = data.results
                        val page = data.page

                        networkState.postValue(Resource.success("loaded"))

                        callback.onResult(
                            movieListMapper.mapRemoteListToModel(items.filterNotNull()) as MutableList<MovieListModel>,
                            0,
                            page + 1
                        )
                    }
                    else{
                        networkState.postValue(Resource.error("failed"))
                    }
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    networkState.postValue(Resource.error("failed"))
                }

            })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MovieListModel>) {
        networkState.postValue(Resource.loading())
        movieRemoteSource.discoverMovies(orderBy, params.key)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : SingleObserver<Response<DiscoverMoviesResponse>> {
                override fun onSuccess(response: Response<DiscoverMoviesResponse>) {
                    if (response.isSuccessful) {
                        val data = response.body()!!
                        val items = data.results
                        val page = data.page

                        networkState.postValue(Resource.success("loaded"))

                        callback.onResult(
                            movieListMapper.mapRemoteListToModel(items.filterNotNull()) as MutableList<MovieListModel>,
                            page + 1
                        )
                    }
                    else{
                        networkState.postValue(Resource.error("failed"))
                    }
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    networkState.postValue(Resource.error(e.localizedMessage))
                }

            })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MovieListModel>) {
        //left empty
    }
}