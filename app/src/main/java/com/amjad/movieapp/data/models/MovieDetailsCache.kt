package com.amjad.movieapp.data.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * The local representation of the caches movie details model
 * @property id Int
 * @property adult Boolean
 * @property backdropPath String?
 * @property budget Int
 * @property genres List<Genre>
 * @property homepage String?
 * @property imdbId String?
 * @property originalLanguage String
 * @property originalTitle String
 * @property overview String?
 * @property popularity Double
 * @property posterPath String?
 * @property releaseDate String
 * @property revenue Int
 * @property runtime Int?
 * @property status String
 * @property tagline String?
 * @property title String
 * @property video Boolean
 * @property voteAverage Double
 * @property voteCount Int
 * @property localCreationDate Long
 * @constructor
 */
@Entity(tableName = "movies", indices = [Index(value = ["id"], unique = true)])
data class MovieDetailsCache (
    @PrimaryKey
    val id: Int,
    val adult: Boolean,
    val backdropPath: String?,
    val budget: Int,
    val genres: List<Genre>,
    val homepage: String?,
    val imdbId: String?,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String?,
    val popularity: Double,
    val posterPath: String?,
    val releaseDate: String,
    val revenue: Int,
    val runtime: Int?,
    val status: String,
    val tagline: String?,
    val title: String,
    val video: Boolean,
    val voteAverage: Double,
    val voteCount: Int,
    val localCreationDate:Long
)


fun MovieDetailsCache.isExpiredAfterOneDay():Boolean{
    val currentDate = java.util.Date()
    val calendar = java.util.Calendar.getInstance()
    calendar.time= java.util.Date(localCreationDate)
    calendar.add(java.util.Calendar.HOUR,24)

    return currentDate >= calendar.time
}