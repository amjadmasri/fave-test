package com.amjad.movieapp.utilities

import com.amjad.movieapp.data.models.MovieDetailsCache
import java.util.*

object TestObjectsProvider {

    fun provideMovieLocalDataModel(id: Int) = MovieDetailsCache(
        id,
        false,
        "",
        0,
        arrayListOf(),
        "",
        "",
        "",
        "",
        "",
        0.1,
        "",
        "",
        0,
        0,
        "",
        "",
        "",
        false,
        0.1,
        1,
        Date().time
    )
}