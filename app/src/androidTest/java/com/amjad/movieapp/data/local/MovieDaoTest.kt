package com.amjad.movieapp.data.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amjad.movieapp.data.cache.AppDatabase
import com.amjad.movieapp.data.cache.dao.MovieDao
import com.amjad.movieapp.data.models.MovieDetailsCache
import com.amjad.movieapp.utilities.LiveDataTestUtil
import com.amjad.movieapp.utilities.TestObjectsProvider
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*


@RunWith(AndroidJUnit4::class)
class SimpleEntityReadWriteTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var movieDao: MovieDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        )
            .allowMainThreadQueries().build()
        movieDao = db.movieDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun getMovieWhenNoMovieInserted() {
        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(true, result == null)
    }

    @Test
    fun getMovieWhenMovieInserted() {
        val movieLocalDataModel = TestObjectsProvider.provideMovieLocalDataModel(1)
        movieDao.insert(movieLocalDataModel)
            .subscribe()

        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(true, result != null)
    }

    @Test
    fun getMovieWhenMovieListInserted() {
        val movieLocalDataModel = TestObjectsProvider.provideMovieLocalDataModel(1)
        movieDao.insertList(arrayListOf(movieLocalDataModel))
            .subscribe()

        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(true, result != null)
    }

    @Test
    fun getMovieWhenMovieListInsertedCheckData() {
        val movieLocalDataModel = TestObjectsProvider.provideMovieLocalDataModel(1)
        movieDao.insertList(arrayListOf(movieLocalDataModel))
            .subscribe()

        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(movieLocalDataModel.id, result?.id)
        Assert.assertEquals(movieLocalDataModel.adult, result?.adult)
        Assert.assertEquals(movieLocalDataModel.backdropPath, result?.backdropPath)
        Assert.assertEquals(movieLocalDataModel.budget, result?.budget)
        Assert.assertEquals(movieLocalDataModel.homepage, result?.homepage)

        Assert.assertEquals(movieLocalDataModel.imdbId, result?.imdbId)
        Assert.assertEquals(movieLocalDataModel.releaseDate, result?.releaseDate)
        Assert.assertEquals(movieLocalDataModel.title, result?.title)
        Assert.assertEquals(movieLocalDataModel.originalTitle, result?.originalTitle)

    }

    @Test
    fun getMovieWhenMovieListInsertedAndItemNotFound() {
        val movieLocalDataModel = TestObjectsProvider.provideMovieLocalDataModel(2)
        movieDao.insertList(arrayListOf(movieLocalDataModel))
            .subscribe()

        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(true, result == null)
    }

    @Test
    fun insertTwoMoviesCheckReplaced() {

        val movieLocalDataModel = TestObjectsProvider.provideMovieLocalDataModel(1)

        val movieLocalDataModel2 = MovieDetailsCache(
            1,
            true,
            "path",
            1000,
            arrayListOf(),
            "homepage",
            "imdbId",
            "en",
            "title",
            "overview",
            1.1,
            "path",
            "date",
            123,
            123,
            "status",
            "tagline",
            "title",
            false,
            1.1,
            1,
            Date().time
        )

        movieDao.insert(movieLocalDataModel)
            .subscribe()

        movieDao.insert(movieLocalDataModel2)
            .subscribe()

        //check result from db directly
        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(result?.title, movieLocalDataModel2.title)
    }

}