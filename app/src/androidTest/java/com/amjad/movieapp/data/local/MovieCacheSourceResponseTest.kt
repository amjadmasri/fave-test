package com.amjad.movieapp.data.local

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amjad.movieapp.data.cache.AppDatabase
import com.amjad.movieapp.data.cache.MovieCacheSource
import com.amjad.movieapp.data.cache.MovieCacheSourceImp
import com.amjad.movieapp.data.cache.dao.MovieDao
import com.amjad.movieapp.data.models.MovieDetailsCache
import com.amjad.movieapp.utilities.LiveDataTestUtil
import io.reactivex.Completable
import org.hamcrest.Matchers.instanceOf
import org.junit.*
import org.junit.Assert.assertThat
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*

@RunWith(AndroidJUnit4::class)
class MovieCacheSourceResponseTest {


    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var movieDao: MovieDao
    private lateinit var db: AppDatabase

    private lateinit var movieCacheSource: MovieCacheSource
    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        )
            .allowMainThreadQueries().build()
        movieDao = db.movieDao()

        movieCacheSource = MovieCacheSourceImp(db)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun testInsertMovieResponseType() {
        val movieLocalDataModel = MovieDetailsCache(
            1,
            true,
            "path",
            1000,
            arrayListOf(),
            "homepage",
            "imdbId",
            "en",
            "title",
            "overview",
            1.1,
            "path",
            "date",
            123,
            123,
            "status",
            "tagline",
            "title",
            false,
            1.1,
            1,
            Date().time
        )
        assertThat(
            movieCacheSource.insertMovieDetails(movieLocalDataModel),
            instanceOf(Completable::class.java)
        )

        movieCacheSource.insertMovieDetails(movieLocalDataModel)
            .subscribe()

        //check result from db directly
        val result = LiveDataTestUtil.getValue(movieDao.getMovieById(1))

        Assert.assertEquals(movieLocalDataModel, result)
    }

    @Test
    fun testGetMovieResponseType() {
        assertThat(movieCacheSource.getMovieDetails(1), instanceOf(LiveData::class.java))
    }

}