# Project Title

Movie App Assignment

## Architecture components used 

-Room (with rxJava extension)
-Navigation
-Paging
-LiveData
-ViewModel

## 3rd party libraries Used
###Design 
-Material Design
###Network 
-Retrofit
-Retrofit rxJava Adapter
-Gson
used retrofit with rxJava Adapter to handle network requests in a reactive manner while deserializing the data using "Gson"
###Dependency Injection
-Dagger2
-Dagger Android
Dagger is one of the best Dependency Injection libraries to use on Android.
Dagger is light weight and it generates code at compile time.
Dagger 2 uses generated code to access the fields and not reflection.

###RX
-RxJava
-RxAndroid
used rx for handling asynchronous network calls and local database access 

## Architecture

In order to make the challenge application 
- Testable
- Scalable
- Modular
- Reusable


I choose to use clean architecture as the architectural pattern to develop this assignment application 
The code is organized in one App module and in it three layers:

- Data:
    - The models that represent both the network responses and the local database data classes.
    - The "sources" by which to get the data  e.g., "Movie Remote Source " handles calling "Retrofit" ApiService for network calls on the "Movie" resource
    - Mappers that handle mapping the models from their data layer representation to the domain layer representation.
    - Repositories implementations for interfaces in the domain layer. the repositories are responsible for example for consuming the 
    rx responses from the network and returning  LiveData instances for the domain layer.
    - DI : the Dependency Injection modules needed in this layer

- Domain:
    - Domain models : in the domain models for this application the data in each model is restricted to only the data that the UI needs to present and the ids as identifiers 
    - Repositories interfaces : interfaces that represent the functionality of each repository
    - in Clean Architecture  Use cases would be used to make functionality reusable and maintainable , but in the case of this assignment they weren't used for simplification purposes as well as Mappers that handle mapping the mapping from domain models to Presentation models(which were used as the same domain models for simplicity).

- Presentation:
    - Presentation models: in this case they are the same as the Domain models 
    - UI: the app uses the single activity multiple Fragments approach and so the UI is organized as such.
    - ViewModels: a ViewModel for each Fragment that uses the use cases to execute functionalities and represent state using the LiveData approach 
    as well as the ViewModel Factory that is responsible for creating any ViewModel.

and finally there is a common package:
- Common Dependency Injection components and modules
- Models: common Models mainly the "Resource" and the "Result" classes that are used in all the layers.
- Utilities: common Utilities that are used in all app layers.

By using Clean Architecture the application is ready to be scaled and ready to add many features without much trouble and save much time later.
Also it is compatible to be used with  MVVM and Android Architecture components and follows the SOLID principles.          

### LiveData usage reasoning
As explained above RX was used in the inner data layer of the application to handle the network calls and the local database access because RX makes handling asynchronous tasks easy and less error prone,
And in the Domain layer and the Presentation layer LiveData was used to observe and move data between layers.
The reason for this decision is that LiveData is well equipped to be integrated easily in the  UI (activities ,Fragments),
it is lifecycle aware and using it to observe data and operations state made sense as a choice.
finally with the usage of MediatorLiveData LiveData is also able to be a solution to pass and observe data in the domain model before 
returning it to the Presentation layer
then also let the ViewModels and eventually the UI observe the results after applying any operations (like mapping or resolving relationships from the API) on the data.

### Caching only Movie Details data reasoning
I decided to cache the result of the movie details API to minimize making network calls on the same resource when accessed multiple times.
an expiration policy was used to determine when the local cache should be used or should the data be refreshed from the remote API
the simple policy is checking if the data has been cached in less that 24 hours. After 24 the data is considered expired and needs a refresh.

